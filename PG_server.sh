# login as user root with the password defined when you created the droplet

# When prompted choose and confirm defaults
# Change all your_* to reasonable values

### Add at least one non-root user (consider having one per groupmember)
adduser your_user
# give admin privileges to the user
gpasswd -a your_user sudo
su - your_user

### Add a non-root user for the lecturer
sudo adduser ninsbl

# Add additional non-root user for co-workers if you like
sudo adduser your_coworkers

# Update package list and installed packages
sudo apt update
sudo apt upgrade

# Configure firewall to keep bots out
# See https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-18-04
# Get your client IP(s), e.g. by visiting https://www.whatismyip.com/
# Allow traffic from the subnets your IP belongs to 
# Replace the last number of your IP with 0/24 if e.g. your IP adress is 84.214.36.12 allow form 84.214.36.0/24
sudo ufw allow from your_ips_subnet.0/24
sudo ufw enable

# Install package sources for pdAdmin4 for database administration
curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'

# Install required packages
sudo apt install postgresql-postgis postgresql-contrib pgmodeler firefox pgadmin4 xfce4 xfce4-goodies tightvncserver novnc python3-dev r-base libapparmor1 gdebi-core websockify python3-numpy python3-pip

### NoVNC(when asked to create/provide password, do so)
vncserver
vncserver -kill :1

echo '#!/bin/sh
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
startxfce4 &' > ~/.vnc/xstartup

vncserver

cd /etc/ssl
sudo openssl req -x509 -nodes -newkey rsa:2048 -keyout novnc.pem -out novnc.pem -days 365
sudo chmod 644 novnc.pem
websockify -D --web=/usr/share/novnc/ --cert=/etc/ssl/novnc.pem 6080 localhost:5901
cd ~

# Access Remote Desktop through NoVNC:
# http://(server's IP address):6080/vnc.html

### PostgreSQL
# https://help.ubuntu.com/community/PostgreSQL
# https://www.tecmint.com/install-postgresql-and-pgadmin-in-ubuntu/
sudo -u postgres createuser --superuser $USER
sudo -u postgres createdb -O $USER hobbyhuset
sudo -u postgres psql
# in psql (promt postgres=#)
\password "your_user"
\q

# Feel free to add other database users users with limited privileges e.g. for co-workers
# see: https://www.postgresql.org/docs/current/sql-createrole.html

# Configure PostgreSQL
ip="ip_of_your_server" # e.g. 64.225.101.212
sudo sed -i "0,/127.0.0.1/s//${ip}/" /etc/postgresql/12/main/pg_hba.conf
sudo sed -i "0,/#listen_addresses = 'localhost'/s//listen_addresses = '*'/"  /etc/postgresql/12/main/postgresql.conf

sudo service restart postgresql
# Create new connection

# Get and load hobbyhuset to PostgreSQL
psql -d hobbyhuset -c "CREATE SCHEMA $USER ;"
wget https://transfer.sh/FybeT/hobbyhuset.sql
psql -d hobbyhuset -f hobbyhuset.sql
rm hobbyhuset.sql


# Start database management tools in remote Desktop
# http://(server's IP address):6080/vnc.html
# Go on Applications -> Terminal -> enter pgmodeler in the command line

# In pgModeler
# Setup (edit) the database connection
# After filling out the form click "Add connection"
# Import the DB model

# Setup web access for pgAdmin
sudo /usr/pgadmin4/bin/setup-web.sh

# Connect to database administration tools
# http://(server's IP address)/pgadmin4

# If you like, feel free to add pgadmin users or your co-workers, so all of you can run queries
# To do so: In the pgadmin user interface, click on the user name in the upper right corner
# to add more users (for your co-workers) so all of 

# Define connection (connection_name=your_free_connection_name, server=your_server_ip, dbname=hobbyhuset, user=your_user, password=your_db_password)

####################
# The following Voluntary
####################

### RStudio and Shiny
# https://deanattali.com/2015/05/09/setup-rstudio-shiny-server-digital-ocean/#create-droplet

# RStudio Server
wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-1.4.1103-amd64.deb
sudo gdebi rstudio-server-1.4.1103-amd64.deb

# Clean up after successful installation
rm rstudio-server-1.4.1103-amd64.deb

# Shiny
sudo su - -c "R -e \"install.packages('shiny', repos='http://cran.rstudio.com/')\""
wget https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.15.953-amd64.deb
sudo gdebi shiny-server-1.5.15.953-amd64.deb

# Clean up after successful installation
rm shiny-server-1.5.15.953-amd64.deb

####################
# The following is extra voluntary and not fully configured
####################

### Jupyter Lab
# https://www.digitalocean.com/community/tutorials/how-to-set-up-a-jupyterlab-environment-on-ubuntu-18-04
sudo apt install python3-notebook jupyter jupyter-core 
jupyter notebook --generate-config
more $HOME/.jupyter/jupyter_notebook_config.py
# Allow remote IP in config file
sed -i -e "s/#c.NotebookApp.allow_origin = ''/c.NotebookApp.allow_origin = '*'/;s/#c.NotebookApp.ip = 'localhost'/c.NotebookApp.ip = '${ip}'/;s/#c.NotebookApp.allow_remote_access = False/c.NotebookApp.allow_remote_access = True/" $HOME/.jupyter/jupyter_notebook_config.py

# Define password authentication
jupyter notebook password
# Start jupyterlab in the background
jupyter notebook &

python3 -m pip install bokeh
