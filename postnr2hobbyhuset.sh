#!/bin/bash

# Requires GDAL binaries
# sudo apt install gdal-bin

# Set environment variable to use COPY instead of INSERT (faster)
export PG_USE_COPY=YES

# Import postal codes from WFS
ogr2ogr -overwrite -lco LAUNDER=YES -nln postomrader PG:"dbname='hobbyhuset'"  WFS:https://wfs.geonorge.no/skwms1/wfs.postnummeromrader?service=wfs? "app:Postnummerområde"
