-----------------------------------------------------------------------------------------------
-- SQL General introduction

/*
Standards evolved SQL-86, SQL-89, ..., SQL:2019
  Pro of using standards: portability, you can switch the DB backend of your application without major pain
  Con of using standards: you probably cannot use all functions of your DBMS

For help also consult PostgreSQL documentation:
https://www.postgresql.org/docs/12/index.html


Important SQL key words:

SELECT - Read/load data
SELECT DISTINCT - removes duplicates from query results
FROM - specify a table or sub-query to read from
WHERE - limit results in a query based on expressions
GROUP BY - define groups for aggregate queries
HAVING - define aggregation that can be used in a where clause
ORDER BY - order results by one pr more columns in ascending (ASC = default) or descending (DESC) order)

UNION - Append results of two (sub-) queries

INSERT - Add one or more new rows to a table
UPDATE - Change values for a row/column in a table
DELETE - Remove one or more rows from a table
COPY - Efficiently move bulks of data e.g. from a DB table to CSV or the other way around 

CREATE TABLE - 
CREATE VIEW - 

ALTER TABLE - Change the definition of a table
ALTER VIEW - Change the definition of a view

DROP VIEW - Delete a view
DROP TABLE - Delete a table

JOIN [INNER, LEFT, RIGHT, FULL, NATURAL, CROSS] - combine two tables based on values in a column

*/

-----------------------------------------------------------------------------------------------
-- Role management and security

-- Create new simple user
-- e.g. for an application that only is supposed to read data from the DB
-- DROP ROLE IF EXISTS dbviewer;
CREATE ROLE dbviewer WITH
	LOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'viewer';

-- Create a new conection in pgAdmin4 with the user "dbviewer"
-- Try to open a table from hobbyhuset in that connection
-- It will fail due to lack of permissions
-- PostgreSQL allows even more fine grained access control with
-- row-level-security: https://www.postgresql.org/docs/12/ddl-rowsecurity.html

GRANT USAGE ON SCHEMA stefan TO dbviewer;
ALTER DEFAULT PRIVILEGES IN SCHEMA stefan
GRANT SELECT ON TABLES TO dbviewer;
GRANT SELECT ON TABLE stefan."Ansatt" TO dbviewer;
GRANT SELECT ON TABLE stefan."Kategori" TO dbviewer;
GRANT SELECT ON TABLE stefan."Kunde" TO dbviewer;
GRANT SELECT ON TABLE stefan."Ordre" TO dbviewer;
GRANT SELECT ON TABLE stefan."Ordrelinje" TO dbviewer;
GRANT SELECT ON TABLE stefan."Poststed" TO dbviewer;
GRANT SELECT ON TABLE stefan."Prishistorikk" TO dbviewer;
GRANT SELECT ON TABLE stefan."Vare" TO dbviewer;

-- Try again to open a table from hobbyhuset in the "dbviewer" connection
-- However, now you cannot drop the role anymore because objects depend on it
-- To do so, you would have to uncomment the follwing two lines:
-- DROP OWNED BY dbviewer;
-- DROP ROLE IF EXISTS dbviewer;

-----------------------------------------------------------------------------------------------
-- Create a new Leverandør table with relations to Poststed and Vare

-- DROP TABLE IF EXISTS stefan."Leverandoer" CASCADE;
CREATE TABLE stefan."Leverandoer" (
	"LeverandoerNr" serial NOT NULL,
	"Navn" character varying(50),
	"PostNr" char(4),
	CONSTRAINT "Leverandoer_pkey" PRIMARY KEY ("LeverandoerNr")

);

-- ALTER TABLE stefan."Vare" DROP COLUMN IF EXISTS "LeverandoerNr" CASCADE;
ALTER TABLE stefan."Vare" ADD COLUMN "LeverandoerNr" integer;

-- ALTER TABLE stefan."Leverandoer" DROP CONSTRAINT IF EXISTS "Leverandoer_postnr_fkey" CASCADE;
ALTER TABLE stefan."Leverandoer" ADD CONSTRAINT "Leverandoer_postnr_fkey" FOREIGN KEY ("PostNr")
REFERENCES stefan."Poststed" ("PostNr") MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ALTER TABLE stefan."Vare" DROP CONSTRAINT IF EXISTS "LeverandoerNr_fkey" CASCADE;
ALTER TABLE stefan."Vare" ADD CONSTRAINT "LeverandoerNr_fkey" FOREIGN KEY ("LeverandoerNr")
REFERENCES stefan."Leverandoer" ("LeverandoerNr") MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;



---------------------------------------------------------------------------------------
-- Constraints (in addition to unique constraints like in primary keys)

-- Non-NULL
ALTER TABLE stefan."Ordre"
    ALTER COLUMN "OrdreDato" SET NOT NULL;

-- Default values (e.g. if omitted in INSERT)
ALTER TABLE stefan."Ordre"
    ALTER COLUMN "OrdreDato" SET DEFAULT CURRENT_DATE;

-- Check constraints
ALTER TABLE stefan."Ordre"
    ADD CONSTRAINT "Ordre_Dato_Send_check" CHECK ("SendtDato" >= "OrdreDato");

ALTER TABLE stefan."Vare"
    ADD CONSTRAINT "Vare_Antall_check" CHECK ("Antall" >= 0);
-- In pgadmin validate the constraint from the contex menu

---------------------------------------------------------------------------------------
-- INSERT
INSERT INTO stefan."Ordrelinje"(
	"OrdreNr", VNr, "PrisPrEnhet", "Antall")
	VALUES (33333, 25154, 19, '5 stk');

-- Column not found: Quote column "VNr"
-- Foreign key violated: Change value for "OrdreNr" to 20515
-- Change value for "Antall" to integer (remove single quotes and " stk")

-- Can you insert an ordrelinje for "VNr" 65033 for "OrdreNr" to 20515?
INSERT INTO stefan."Ordrelinje"(
	"OrdreNr", "VNr", "PrisPrEnhet", "Antall")
	VALUES (20515, 65033, 217.99, 3);

---------------------------------------------------------------------------------------
-- SELECT

-- When you explain / analyse the following query (a button in pgadmin4),
-- note the sequence scan (entire table is read and checked for dates)
SELECT	"OrdreNr",
		"OrdreDato",
		"SendtDato",
		"BetaltDato",
		"KNr"
FROM	stefan."Ordre"
WHERE "OrdreDato" > '2020-01-01';


-- When you explain / analyse the following query (a button in pgadmin4),
-- note the index scan (efficient data access) and reduced query time
SELECT	"OrdreNr",
		"OrdreDato",
		"SendtDato",
		"BetaltDato",
		"KNr"
FROM	stefan."Ordre"
WHERE "OrdreNr" IN (22598, 22599);


---------------------------------------------------------------------------------------
-- UPDATE

-- Check the row we want to change
SELECT * FROM stefan."Vare" WHERE "VNr" = '10822';

-- Change a value
UPDATE stefan."Vare"
	SET "Betegnelse" = 'Dukkehår, brunt'
	WHERE "VNr" = '10822';

-- Check the row we want to change again
SELECT * FROM stefan."Vare" WHERE "VNr" = '10822';

-- Note: Changing values in keys requires you to safeguard referential integrity

---------------------------------------------------------------------------------------
-- DELETE

DELETE FROM stefan."Ordre"
	WHERE "OrdreNr" = 20539;

-- fails due to referential integrity, the following would work, but also delete rows from "Ordrelinje"
-- DELETE FROM stefan."Ordre"
-- 	WHERE "OrdreNr" = 20539 CASCADE;

---------------------------------------------------------------------------------------
-- Analytical queries

-- Analyze the number of customers per month
SELECT		EXTRACT(MONTH FROM "OrdreDato") AS maaned,
			count("KNr") AS antall_kunder
FROM		stefan."Ordre" NATURAL LEFT JOIN
			stefan."Kunde"
GROUP BY	EXTRACT(MONTH FROM "OrdreDato");

-- Analyze the number of individual customers per month
SELECT 		CAST(count("KNr") AS integer) AS antall_kunder,
			maaned
FROM 		(
SELECT		DISTINCT ON ("KNr", EXTRACT(MONTH FROM "OrdreDato"))
			"KNr",
			CAST(EXTRACT(MONTH FROM "OrdreDato") AS smallint) AS maaned
FROM		stefan."Ordre") AS a
GROUP BY	maaned
ORDER BY	maaned



-- Analyze maximum, average, and minimum sales volum per month and year
-- using a sub-query
SELECT		CAST(maaned AS smallint),
			round(max(volum), 2) AS volum_max,
			round(avg(volum), 2) AS volum_avg,
			round(min(volum), 2) AS volum_min
FROM		(
SELECT		EXTRACT(YEAR FROM "OrdreDato") AS aar,
			EXTRACT(MONTH FROM "OrdreDato") AS maaned,
			sum("Antall" * "PrisPrEnhet") AS volum
FROM		stefan."Ordre" NATURAL LEFT JOIN
			stefan."Ordrelinje"
GROUP BY	aar, EXTRACT(MONTH FROM "OrdreDato")) AS ma
GROUP BY	maaned
ORDER BY	maaned;


---------------------------------------------------------------------------------------
-- Views and Materialized Views

-- Analyze the number of customers per month
-- DROP VIEW IF EXISTS stefan.kunder_maaned
CREATE OR REPLACE VIEW stefan.kunder_maaned AS
SELECT		EXTRACT(MONTH FROM "OrdreDato") AS maaned,
			count("KNr") AS antall_kunder
FROM		stefan."Ordre" NATURAL LEFT JOIN
			stefan."Kunde"
GROUP BY	EXTRACT(MONTH FROM "OrdreDato");

-- DROP MATERIALIZED VIEW IF EXISTS stefan.kunder_maaned_materialized;
CREATE MATERIALIZED VIEW stefan.kunder_maaned_materialized AS
SELECT		EXTRACT(MONTH FROM "OrdreDato") AS maaned,
			count("KNr") AS antall_kunder
FROM		stefan."Ordre" NATURAL LEFT JOIN
			stefan."Kunde"
GROUP BY	EXTRACT(MONTH FROM "OrdreDato");

-- Explain
SELECT * FROM stefan.kunder_maaned;
-- vs
SELECT * FROM stefan.kunder_maaned_materialized;

-- Compare results
SELECT * FROM stefan.kunder_maaned;
-- vs
SELECT * FROM stefan.kunder_maaned_materialized;

-- Insert new orders
INSERT INTO stefan."Ordre"(
	"OrdreNr", "OrdreDato", "SendtDato", "BetaltDato", "KNr")
	VALUES
	(23000, '2020-01-09', '2020-01-09', '2020-01-10', 5427),
	(23001, '2020-01-09', '2020-01-09', '2020-01-10', 5427)
	(23002, '2020-01-09', '2020-01-09', '2020-01-10', 5427)
	(23003, '2020-01-09', '2020-01-09', '2020-01-10', 5427);
	
-- Compare results again for January
SELECT * FROM stefan.kunder_maaned;
-- vs
SELECT * FROM stefan.kunder_maaned_materialized;
