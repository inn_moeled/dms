# Install packages (if needed)
install.packages("RPostgreSQL") # Requires libpq-dev system package (sudo apt install libpq-dev)
install.packages("pool")

# Load packages
library(RPostgreSQL)
library(pool)

# Set connection parameters
pg_drv <- RPostgreSQL::PostgreSQL()
pg_host <- "your_droplet_ip"
pg_db <- 'hobbyhuset'
pg_user <- "dbviewer"
pg_password <- "viewer"


# Define connection pool
pool <- dbPool(
  drv = pg_drv,
  dbname = pg_db,
  host = pg_host,
  user = pg_user,
  password = pg_password,
  idleTimeout = 36000000
)

# Get a connection
con <- poolCheckout(pool)

# Run a query
sql_string <- "SELECT * FROM stefan.kunder_maaned_materialized;"
df <- dbGetQuery(con, sql_string)

# Close pool
poolClose(pool)

# Plot results
plot(antall_kunder ~ maaned, data=df)