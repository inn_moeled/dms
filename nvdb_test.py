#!/usr/bin/python3

# Examples from:
# https://github.com/LtGlahn/nvdbapi-V3

from nvdbapiv3 import *

# Example "Døgnhvileplass"
p = nvdbFagdata( 809) # Døgnhvileplass 
p.paginering['antall'] = 3 # Jukser litt med antall forekomster per bøtte. 
TF = p.nestePaginering()
minliste = []
while TF:
    minliste.extend( p.data['objekter'] )
    TF = p.nestePaginering()

print(minliste)

# Example for Object oriented handling of responses to API calls
fart = finnid(85288328, kunfagdata=True) # python-dict
fartobj = nvdbFagObjekt(fart)   # Objektorientert representasjon, se definisjonen nvdbFagobjekt

print(fartobj)
